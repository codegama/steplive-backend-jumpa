<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{
    protected $fillable = ['user_id', 'title', 'schedule_time'];

	protected $hidden = ['deleted_at', 'id', 'unique_id'];

	protected $appends = ['meeting_id','meeting_unique_id', 'no_of_users_formatted', 'no_of_minutes_formatted', 'status_formatted', 'is_recordings_formatted'];

    public function getMeetingIdAttribute() {

        return $this->id;
    }

    public function getMeetingUniqueIdAttribute() {

        return $this->unique_id;
    }

    public function getNoOfUsersFormattedAttribute() {

        return no_of_users_formatted($this->no_of_users);
    }

    public function getNoOfMinutesFormattedAttribute() {

        return no_of_minutes_formatted($this->no_of_minutes);
    }

    public function getStatusFormattedAttribute() {

        return meeting_status($this->status);
    }

    public function getIsRecordingsFormattedAttribute() {

        return $this->is_recordings == YES ? tr('yes') : tr('no');
    }

	public function meetingUsers() {

		return $this->hasMany(MeetingUser::class,'meeting_id');
	}

    public function userDetails() {

        return $this->belongsTo(User::class,'user_id');
    }


    public function meetingRecords() {

        return $this->hasMany(MeetingRecord::class,'meeting_id');
    }

	/**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCommonResponse($query) {
        return $query;
    }

    public static function boot() {

        parent::boot();

        static::creating(function ($model) {

            $model->attributes['unique_id'] = date('dmYHis'); 
        });

        static::created(function($model) {

            // $model->attributes['unique_id'] = "MT"."-".$model->attributes['id']."-".uniqid();
            $model->attributes['unique_id'] = date('dmYHis');

            $model->save();
        
        });

        static::deleting(function ($model){

            \Helper::storage_delete_file($model->picture, COMMON_FILE_PATH); 

            foreach ($model->meetingUsers as $key => $meeting_users) {
                
                $meeting_users->delete();

            }

            foreach ($model->meetingRecords as $key => $meeting_records) {
                
                $meeting_records->delete();

            }

        });

    }
}
