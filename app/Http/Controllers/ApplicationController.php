<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use DB, Hash, Setting, Auth, Validator, Exception, Enveditor, Helper;

class ApplicationController extends Controller
{
    /**
     * @method static_pages_api()
     *
     * @uses used to get the pages
     *
     * @created Vidhya R 
     *
     * @edited Vidhya R
     *
     * @param - 
     *
     * @return JSON Response
     */

    public function static_pages_api(Request $request) {

        $base_query = \App\StaticPage::where('status', APPROVED)->CommonResponse();

        if($request->page_type) {

            $static_pages = $base_query->where('type', $request->page_type)->first();

        } elseif($request->page_id) {

            $static_pages = $base_query->where('id', $request->page_id)->first();

        } elseif($request->unique_id) {

            $static_pages = $base_query->where('unique_id', $request->unique_id)->first();

        } else {

            $static_pages = $base_query->get();

        }

        return $this->sendResponse($message = "", $code = "", $static_pages);

    }
    /**
     * @method email_verify()
     *
     * @uses To verify the email from user and provider.  
     *
     * @created Bhawya
     *
     * @updated Bhawya
     *
     * @param -
     *
     * @return JSON RESPONSE
     */

    public function email_verify(Request $request) {

        if($request->user_id) {

            $user_details = User::find($request->user_id);

            if(!$user_details) {

                return redirect()->away(Setting::get('frontend_url'))->with('flash_error',tr('user_details_not_found'));
            } 

            if($user_details->is_verified == USER_EMAIL_VERIFIED) {

                return redirect()->away(Setting::get('frontend_url'))->with('flash_success' ,tr('verify_success'));
            }

            $response = Helper::check_email_verification($request->verification_code , $user_details->id, $error, USER);
            
            if($response) {

                $user_details->is_verified = USER_EMAIL_VERIFIED;       

                $user_details->save();

            } else {

                return redirect()->away(Setting::get('frontend_url'))->with('flash_error' , $error);
            }

        } 
        
        return redirect()->away(Setting::get('frontend_url'));
    
    }

    /**
     * @method support_contacts_save()
     *
     * @uses used to get the pages
     *
     * @created Vidhya R 
     *
     * @edited Vidhya R
     *
     * @param - 
     *
     * @return JSON Response
     */

    public function support_contacts_save(Request $request) {

        try {

            DB::beginTransaction();

            $rules =
                [
                    'name' => 'required',
                    'email' => 'required|email',
                    'mobile' => 'required',
                    'country' => '',
                    'message' => 'required',
                ];

            Helper::custom_validator($request->all(),$rules, $custom_errors = []);

            $support_contact = new \App\SupportContact;

            $support_contact->unique_id = rand();

            $support_contact->user_id = $request->id ?: 0;

            $support_contact->name = $request->name ?: "";

            $support_contact->email = $request->email ?: "";

            $support_contact->mobile = $request->mobile ?: "";

            $support_contact->country = $request->country ?: "";

            $support_contact->message = $request->message ?: "";

            $support_contact->save();           

            DB::commit();

            return $this->sendResponse($message = api_success(126), $code = 126, $support_contact);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

}
