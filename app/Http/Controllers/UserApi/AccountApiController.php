<?php

namespace App\Http\Controllers\UserApi;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use DB, Log, Hash, Validator, Exception, Setting, Helper;

use App\Repositories\PaymentRepository as PaymentRepo;

use App\User, App\Subscription, App\SubscriptionPayment, App\UserCard;

use Carbon\Carbon;

use Illuminate\Support\Facades\Password;

use Illuminate\Contracts\Auth\PasswordBroker;

use Illuminate\Auth\Events\PasswordReset;

use Illuminate\Support\Str;


use App\Notifications\ResetPassword as ResetPasswordNotification;


class AccountApiController extends Controller
{
    protected $loginUser;

    protected $skip, $take;

    public function __construct(Request $request) {

        Log::info(url()->current());

        Log::info("Request Data".print_r($request->all(), true));
        
        $this->loginUser = User::find($request->id);

        $this->skip = $request->skip ?: 0;

        $this->take = $request->take ?: (Setting::get('admin_take_count') ?: TAKE_COUNT);

        $this->timezone = $this->loginUser->timezone ?? "America/New_York";

    }

    /**
     * @method register()
     *
     * @uses Registered user can register through manual or social login
     * 
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param Form data
     *
     * @return Json response with user details
     */
    public function register(Request $request) {

        try {
        
            DB::beginTransaction();

            $rules = 
                [
                    'device_type' => 'required|in:'.DEVICE_ANDROID.','.DEVICE_IOS.','.DEVICE_WEB,
                    'device_token' => '',
                    'login_by' => 'required|in:manual,facebook,google,apple,linkedin,instagram',
                ];

            Helper::custom_validator($request->all(), $rules);

            $allowed_social_logins = ['facebook','google','apple', 'linkedin', 'instagram'];
            
            $demo_logins = explode(',', Setting::get('demo_logins'));

            if(in_array($request->login_by, $allowed_social_logins)) {

                // validate social registration fields

                $rules = [
                    'social_unique_id' => 'required',
                    'name' => 'required|max:255|min:2',
                    'email' => in_array($request->email, $demo_logins) ? 'required|email' :'required|email' ,
                    'picture' => '',
                    'gender' => 'in:male,female,others',
                ];

                Helper::custom_validator($request->all(), $rules);

            } else {

                $rules = [
                        'name' => 'required|max:255',
                        'email' => in_array($request->email, $demo_logins) ? 'required|email' :'required|email' ,
                        'password' => 'required|min:6',
                        'mobile' => $request->mobile ? 'digits_between:6,13':'nullable',
                        'picture' => 'mimes:jpeg,jpg,bmp,png',
                    ];

                Helper::custom_validator($request->all(), $rules);

                // validate email existence

                $rules = ['email' => 'unique:users,email'];

                Helper::custom_validator($request->all(), $rules);

            }

            $user_details = User::where('email' , $request->email)->first();

            $send_email = NO;

            // Creating the user

            if(!$user_details) {

                $user_details = new User;

                register_mobile($request->device_type);

                $send_email = YES;

                $user_details->picture = asset('placeholder.jpeg');

                $user_details->registration_steps = 1;

            } else {

                if(in_array($user_details->status, [USER_PENDING , USER_DECLINED])) {

                    throw new Exception(api_error(1000), 1000);
                
                }

            }

            $user_details->name = $request->name ?? "";

            $user_details->email = $request->email ?? "";

            $user_details->mobile = $request->mobile ?? "";

            if($request->has('password')) {

                $user_details->password = Hash::make($request->password ?: "123456");

            }

            $user_details->gender = $request->has('gender') ? $request->gender : "male";

            $check_device_exist = User::where('device_token', $request->device_token)->first();

            if($check_device_exist) {

                $check_device_exist->device_token = "";

                $check_device_exist->save();
            }

            $user_details->device_token = $request->device_token ?: "";

            $user_details->device_type = $request->device_type ?: DEVICE_WEB;

            $user_details->login_by = $request->login_by ?: 'manual';

            $user_details->social_unique_id = $request->social_unique_id ?: '';
            
            $user_details->timezone = $request->timezone ?: '';

            // Upload picture

            if($request->login_by == 'manual') {

                if($request->hasFile('picture')) {

                    $user_details->picture = Helper::storage_upload_file($request->file('picture') , PROFILE_PATH_USER);

                }

            } else {

                $user_details->picture = $request->picture ?: $user_details->picture;

            }   

            if($user_details->save()) {

                $user_details->save();

                // Send welcome email to the new user:

                if($send_email) {

                    if($user_details->login_by == 'manual') {

                        $user_details->password = $request->password;

                        $email_data['subject'] = tr('user_welcome_title' , Setting::get('site_name'));

                        $email_data['email']  = $user_details->email;

                        $email_data['page'] = "emails.users.welcome";

                        // $this->dispatch(new \App\Jobs\SendEmailJob($email_data));

                    }

                }

                if(in_array($user_details->status , [USER_DECLINED , USER_PENDING])) {
                
                    $response = ['success' => false , 'error' => api_error(1000) , 'error_code' => 1000];

                    DB::commit();

                    return response()->json($response, 200);
               
                }

                if($user_details->is_verified == USER_EMAIL_VERIFIED) {

                    $data = User::find($user_details->id);

                    $data->no_of_users = Setting::get('max_no_of_users', 3);

                    $data->no_of_minutes = Setting::get('max_no_of_minutes', 30);

                    $subscription_payment = \App\SubscriptionPayment::where('user_id', $user_details->id)->where('is_current_subscription', YES)->first();

                    if($subscription_payment) {

                        $data->no_of_users = $subscription_payment->no_of_users ?? 5;

                        $data->no_of_minutes = $subscription_payment->no_of_minutes ?? 5;

                    }

                    $message = $user_details->name."! Welcome to ".Setting::get('site_name');

                    $response = ['success' => true, 'message' => $message,'data' => $data];

                } else {

                    $response = ['success' => false, 'error' => api_error(1001), 'error_code'=> 1001];

                    DB::commit();

                    return response()->json($response, 200);

                }

            } else {

                throw new Exception(api_error(103), 103);

            }

            DB::commit();

            return response()->json($response, 200);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }
   
    }

    /**
     * @method login()
     *
     * @uses Registered user can login using their email & password
     * 
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request - User Email & Password
     *
     * @return Json response with user details
     */
    public function login(Request $request) {

        try {

            DB::beginTransaction();

            $basic_validator = Validator::make($request->all(),
                [
                    'device_token' => '',
                    'device_type' => 'required|in:'.DEVICE_ANDROID.','.DEVICE_IOS.','.DEVICE_WEB,
                    'login_by' => 'required|in:manual,facebook,google,apple,linkedin,instagram',
                ]
            );

            if($basic_validator->fails()){

                $error = implode(',', $basic_validator->messages()->all());

                throw new Exception($error , 101);

            }

            /** Validate manual login fields */

            $demo_logins = explode(',', Setting::get('demo_logins'));

            $manual_validator = Validator::make($request->all(),
                [
                    'email' => in_array($request->email, $demo_logins) ? 'required|email' :'required|email' ,
                    'password' => 'required',
                ]
            );

            if($manual_validator->fails()) {

                $error = implode(',', $manual_validator->messages()->all());

                throw new Exception($error , 101);

            }

            $user_details = User::where('email', '=', $request->email)->first();

            $is_email_verified = YES;

            // Check the user details 

            if(!$user_details) {

                throw new Exception(api_error(1002), 1002);

            }

            // check the user approved status

            if($user_details->status != USER_APPROVED) {

                throw new Exception(api_error(1000), 1000);

            }

            if(Setting::get('is_account_email_verification') == YES && !$user_details->is_verified) {

                Helper::check_email_verification("" , $user_details->id, $error);

                $is_email_verified = NO;

            }

            if(!$is_email_verified) {

                throw new Exception(api_error(1001), 1001);
            }

            if(Hash::check($request->password, $user_details->password)) {

                // Generate new tokens
                
                $user_details->token = $user_details->email != 'user@zoom.com' ? Helper::generate_token() : $user_details->token;

                $user_details->token_expiry = Helper::generate_token_expiry();
                
                // Save device details

                $check_device_exist = User::where('device_token', $request->device_token)->first();

                if($check_device_exist) {

                    $check_device_exist->device_token = "";
                    
                    $check_device_exist->save();
                }

                $user_details->device_token = $request->device_token ?? $user_details->device_token;

                $user_details->device_type = $request->device_type ?? $user_details->device_type;

                $user_details->login_by = $request->login_by ?? $user_details->login_by;

                $user_details->timezone = $request->timezone ?? $user_details->timezone;

                $user_details->save();

                $data = User::find($user_details->id);

                $data->no_of_users = Setting::get('max_no_of_users', 3);

                $data->no_of_minutes = Setting::get('max_no_of_minutes', 30);

                $subscription_payment = \App\SubscriptionPayment::where('user_id', $user_details->id)->where('is_current_subscription', YES)->first();

                if($subscription_payment) {

                    $data->no_of_users = $subscription_payment->no_of_users ?? 5;

                    $data->no_of_minutes = $subscription_payment->no_of_minutes ?? 5;

                }

                counter(); // Don't remove 
                
                DB::commit();

                return $this->sendResponse(api_success(101), 101, $data);

            } else {

                throw new Exception(api_error(102), 102);

            }

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }
    
    }

    /**
     * @method forgot_password()
     *
     * @uses If the user forgot his/her password he can hange it over here
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request - Email id
     *
     * @return send mail to the valid user
     */
    
    public function forgot_password(Request $request) {

        try {

            DB::beginTransaction();

            // Check email configuration and email notification enabled by admin

            if(Setting::get('is_email_notification') != YES ) {

                throw new Exception(api_error(106), 106);
                
            }
            
            $rules = ['email' => 'required|email|exists:users,email']; 

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            $user_details = User::where('email' , $request->email)->first();

            if(!$user_details) {

                throw new Exception(api_error(1002), 1002);
            }

            if($user_details->login_by != 'manual') {

                throw new Exception(api_error(118), 118);
                
            }

            // check email verification

            if($user_details->is_verified == USER_EMAIL_NOT_VERIFIED) {

                throw new Exception(api_error(1001), 1001);
            }

            // Check the user approve status

            if(in_array($user_details->status , [USER_DECLINED , USER_PENDING])) {
                throw new Exception(api_error(1000), 1000);
            }

            $token = app('auth.password.broker')->createToken($user_details);

            \App\PasswordReset::where('email', $user_details->email)->delete();

            \App\PasswordReset::insert([
                'email' => $user_details->email,
                'token' => $token,
                'created_at' => Carbon::now()
            ]);

            $email_data['subject'] = tr('forgot_email_title' , Setting::get('site_name'));

            $email_data['email']  = $user_details->email;

            $email_data['name']  = $user_details->name;

            $email_data['page'] = "emails.users.forgot-password";

            $email_data['url'] = Setting::get('frontend_url')."reset-password/".$token;
            
            $this->dispatch(new \App\Jobs\SendEmailJob($email_data));

            DB::commit();

            return $this->sendResponse(api_success(102), $success_code = 102, $data = []);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        }
    
    }




    /**
     * @method reset_password()
     *
     * @uses To reset the password
     *
     * @created Ganesh
     *
     * @updated Ganesh
     *
     * @param object $request - Email id
     *
     * @return send mail to the valid user
     */
    
    public function reset_password(Request $request) {

        try {

            $rules = [
                'password' => 'required|confirmed|min:6',
                'reset_token' => 'required|string',
                // 'email' => 'required|email',
                'password_confirmation'=>'required'
            ]; 

            Helper::custom_validator($request->all(), $rules, $custom_errors =[]);

            DB::beginTransaction();

            $password_reset = \App\PasswordReset::where('token', $request->reset_token)->first();

            if(!$password_reset){

                throw new Exception(api_error(145), 145);
            }
            
            $user = User::where('email', $password_reset->email)->first();

            $user->password = \Hash::make($request->password);

            $user->save();

            \App\PasswordReset::where('email', $user->email) ->delete();

            DB::commit();

            $data = $user;

            $data->no_of_users = Setting::get('max_no_of_users', 3);

            $data->no_of_minutes = Setting::get('max_no_of_minutes', 30);

            $subscription_payment = \App\SubscriptionPayment::where('user_id', $user->id)->where('is_current_subscription', YES)->first();

            if($subscription_payment) {

                $data->no_of_users = $subscription_payment->no_of_users ?? 5;

                $data->no_of_minutes = $subscription_payment->no_of_minutes ?? 5;

            }

            return $this->sendResponse(api_success(128), $success_code = 128, $data);

        } catch(Exception $e) {

             DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        }


   }


    /**
     * @method change_password()
     *
     * @uses To change the password of the user
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request - Password & confirm Password
     *
     * @return json response of the user
     */
    public function change_password(Request $request) {

        try {

            DB::beginTransaction();

            $rules = [
                'password' => 'required|confirmed|min:6',
                'old_password' => 'required|min:6',
            ]; 

            Helper::custom_validator($request->all(), $rules, $custom_errors =[]);

            $user_details = User::find($request->id);

            if(!$user_details) {

                throw new Exception(api_error(1002), 1002);
            }

            if($user_details->login_by != "manual") {

                throw new Exception(api_error(118), 118);
                
            }

            if(Hash::check($request->old_password, $user_details->password)) {

                $user_details->password = Hash::make($request->password);
                
                if($user_details->save()) {

                    DB::commit();

                    $email_data['subject'] = tr('change_password_email_title' , Setting::get('site_name'));

                    $email_data['email']  = $user_details->email;

                    $email_data['page'] = "emails.users.change-password";

                    $this->dispatch(new \App\Jobs\SendEmailJob($email_data));

                    return $this->sendResponse(api_success(104), $success_code = 104, $data = []);
                
                } else {

                    throw new Exception(api_error(103), 103);   
                }

            } else {

                throw new Exception(api_error(108) , 108);
            }

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }

    }

    /** 
     * @method profile()
     *
     * @uses To display the user details based on user  id
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request - User Id
     *
     * @return json response with user details
     */

    public function profile(Request $request) {

        try {

            $user_details = User::where('id', $request->id)->first();

            if(!$user_details) { 

                throw new Exception(api_error(1002) , 1002);
            }

            $user_details->no_of_users = Setting::get('max_no_of_users', 3);

            $user_details->no_of_minutes = Setting::get('max_no_of_minutes', 30);

            $subscription_payment = \App\SubscriptionPayment::where('user_id', $request->id)->where('is_current_subscription', YES)->first();

            if($subscription_payment) {

                $user_details->no_of_users = $subscription_payment->no_of_users ?? 5;

                $user_details->no_of_minutes = $subscription_payment->no_of_minutes ?? 5;

            }

            return $this->sendResponse($message = "", $success_code = "", $user_details);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());

        }
    
    }
 
    /**
     * @method update_profile()
     *
     * @uses To update the user details
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param objecct $request : User details
     *
     * @return json response with user details
     */
    public function update_profile(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $demo_logins = explode(',', Setting::get('demo_logins'));

            $rules = [
                    'name' => 'nullable|max:255',
                    'email' => in_array($request->email, $demo_logins) ? 'nullable|email|unique:users,email,'.$request->id.'|max:255' : 'nullable|email|unique:users,email,'.$request->id,
                    'mobile' => $request->mobile ? 'digits_between:6,13':'nullable',
                    'picture' => 'nullable|mimes:jpeg,bmp,png',
                    'gender' => 'nullable|in:male,female,others',
                    'ios_theme' => 'nullable|numeric|in:0,1,2',
            ];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            // Validation end
            
            $user_details = User::find($request->id);

            if(!$user_details) { 

                throw new Exception(api_error(1002) , 1002);
            }

            $user_details->name = $request->name ?: $user_details->name;
            
            if($request->has('email')) {

                $user_details->email = $request->email;
            }

            $user_details->mobile = $request->mobile ?: $user_details->mobile;

            $user_details->gender = $request->gender ?: $user_details->gender;

            $user_details->address = $request->address ?: $user_details->address;

            $user_details->about = $request->about ?: $user_details->about;

            $user_details->ios_theme = $request->ios_theme ?? $user_details->ios_theme;

            if($request->dob) {
                $user_details->dob = date('Y-m-d', strtotime($request->dob));
            }

            // Upload picture
            if($request->hasFile('picture') != "") {

                Helper::storage_delete_file($user_details->picture, PROFILE_PATH_USER); // Delete the old pic

                $user_details->picture = Helper::storage_upload_file($request->file('picture'), PROFILE_PATH_USER);
            }

            // Upload picture
            if($request->hasFile('cover') != "") {

                Helper::storage_delete_file($user_details->cover, PROFILE_PATH_USER); // Delete the old pic

                $user_details->cover = Helper::storage_upload_file($request->file('cover'), PROFILE_PATH_USER);

            }

            if($user_details->save()) {

                $data = User::find($user_details->id);

                DB::commit();

                return $this->sendResponse($message = api_success(111), $success_code = 111, $data);

            } else {    

                throw new Exception(api_error(103), 103);
            }

        } catch (Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }
   
    }

    /**
     * @method update_profile_picture()
     *
     * @uses To update the user details
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param objecct $request : User details
     *
     * @return json response with user details
     */
    public function update_profile_picture(Request $request) {

        try {

            DB::beginTransaction();
            
            $user = User::find($request->id);

            if(!$user) { 

                throw new Exception(api_error(1002) , 1002);
            }

            $folder_path = storage_path("app/public/".PROFILE_PATH_USER);

            if (!file_exists($folder_path)) {

                mkdir($folder_path, 666, true);
            }

            $file_name = Helper::file_name().'.jpg';

            $path = $folder_path.$file_name;

            \Image::make(file_get_contents($request->picture))->save($path);

            $storage_file_path = PROFILE_PATH_USER.$file_name;

            $old_picture = $user->picture;

            $user->picture = asset(\Storage::url($storage_file_path));

            if($user->save()) {

                Helper::storage_delete_file($old_picture, PROFILE_PATH_USER); // Delete the old pic

                $data = User::find($user->id);

                DB::commit();

                return $this->sendResponse($message = api_success(111), $success_code = 111, $data);

            } else {    

                throw new Exception(api_error(103), 103);
            }

        } catch (Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }
   
    }

    /**
     * @method delete_account()
     * 
     * @uses Delete user account based on user id
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request - Password and user id
     *
     * @return json with boolean output
     */

    public function delete_account(Request $request) {

        try {

            DB::beginTransaction();

            $request->request->add([ 
                'login_by' => $this->loginUser ? $this->loginUser->login_by : "manual",
            ]);

            // Validation start

            $rules = ['password' => 'required_if:login_by,manual'];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            // Validation end

            $user_details = User::find($request->id);

            if(!$user_details) {

                throw new Exception(api_error(1002), 1002);
                
            }

            // The password is not required when the user is login from social. If manual means the password is required

            if($user_details->login_by == 'manual') {

                if(!Hash::check($request->password, $user_details->password)) {
         
                    throw new Exception(api_error(142), 142); 
                }
            
            }

            if($user_details->delete()) {

                DB::commit();

                return $this->sendResponse(api_success(103), $success_code = 103, $data = []);

            } else {

                throw new Exception(api_error(119), 119);
            }

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        }

    }

    /**
     * @method logout()
     *
     * @uses Logout the user
     *
     * @created Vithya R
     *
     * @updated Vithya R
     *
     * @param 
     * 
     * @return
     */
    public function logout(Request $request) {

        return $this->sendResponse(api_success(106), 106);

    }

    /**
     * @method subscriptions_index()
     *
     * @uses To display all the subscription plans
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function subscriptions_index(Request $request) {

        try {

            $base_query = Subscription::Approved()->orderBy('amount', 'asc');

            $free_subscriptions = Subscription::Approved()->where('is_free', YES)->pluck('id');

            $check_user_free_subscriptions = SubscriptionPayment::whereIn('subscription_id', $free_subscriptions)->where('user_id', $request->id)->count();

            if($check_user_free_subscriptions) {

                $base_query = $base_query->where('is_free', NO);
            }

            $subscriptions = $base_query->get();

            $current_subscription =  \App\SubscriptionPayment::where('user_id', $request->id)->where('is_current_subscription', YES)->first();

            foreach ($subscriptions as $key => $value) {

                $value->is_current_subscription = NO;

                if($current_subscription) {

                    $value->is_current_subscription = $value->subscription_id == $current_subscription->subscription_id ? YES : NO;
                }
                
            }

            $data['current_subscription'] = $current_subscription;

            $data['subscriptions'] = $subscriptions ?? [];

            $data['total'] = $subscriptions->count() ?? 0;

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method subscriptions_view()
     *
     * @uses get the selected subscription details
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param integer $subscription_id
     *
     * @return JSON Response
     */
    public function subscriptions_view(Request $request) {

        try {

            $rules = ['subscription_id' => 'required|exists:subscriptions,id'];

            Helper::custom_validator($request->all(), $rules);
            
            $subscription_details = Subscription::Approved()->where('subscriptions.id', $request->subscription_id)->first();

            if(!$subscription_details) {
                throw new Exception(api_error(135), 135);   
            }

            return $this->sendResponse($message = '' , $code = '', $subscription_details);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method subscriptions_history()
     *
     * @uses get the selected subscription details
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param integer $subscription_id
     *
     * @return JSON Response
     */
    public function subscriptions_history(Request $request) {

        try {

            $base_query = $total_query = SubscriptionPayment::where('user_id' , $request->id)->orderBy('subscription_payments.id', 'desc');

            $user_subscriptions = $base_query->skip($this->skip)->take($this->take)->get();

            foreach($user_subscriptions as $key => $value) {

                $value->expiry_date = common_date($value->expiry_date, $this->timezone ?? '', 'd M Y');

                $value->paid_date = common_date($value->paid_date, $this->timezone ?? '', 'd M Y');

                if($value->is_current_subscription == YES) {

                    $value->cancel_btn_status = $value->is_autorenewal ? YES : NO;

                }            
            }

            $data['history'] = $user_subscriptions ?? [];

            $data['total'] = $total_query->count() ?? 0;

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /** 
     * @method subscriptions_payment_by_stripe()
     *
     * @uses pay for subscription using card
     *
     * @created Vidhya R
     *
     * @updated Vidhya R
     *
     * @param
     * 
     * @return JSON response
     *
     */

    public function subscriptions_payment_by_stripe(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = ['subscription_id' => 'required|exists:subscriptions,id'];

            $custom_errors = ['subscription_id' => api_error(138)];

            Helper::custom_validator($request->all(), $rules, $custom_errors);
            
            // Validation end

           // Check the subscription is available

            $subscription_details = Subscription::where('id',  $request->subscription_id)
                                    ->Approved()
                                    ->first();

            if(!$subscription_details) {

                throw new Exception(api_error(138), 138);
                
            }

            $request->request->add(['payment_mode' => CARD]);

            $total = $user_pay_amount = $subscription_details->amount ?? 0.00;


            $request->request->add([
                'total' => $total, 
                'user_pay_amount' => $user_pay_amount,
                'paid_amount' => $user_pay_amount,
            ]);

            if($user_pay_amount > 0) {

                // Check the user have the cards

                $card_details = \App\UserCard::where('user_id', $request->id)->where('is_default', YES)->first();

                // If the user doesn't have cards means the payment will switch to COD
               
                if(!$card_details) {

                    throw new Exception(api_error(136), 136); 

                }

                $request->request->add([
                    'customer_id' => $card_details->customer_id,
                    'card_token' => $card_details->card_token,
                ]);
                
                $card_payment_response = PaymentRepo::subscriptions_payment_by_stripe($request, $subscription_details)->getData();

                if($card_payment_response->success == false) {

                    throw new Exception($card_payment_response->error, $card_payment_response->error_code);
                    
                }

                $card_payment_data = $card_payment_response->data;

                $request->request->add(['paid_amount' => $card_payment_data->paid_amount, 'payment_id' => $card_payment_data->payment_id, 'paid_status' => $card_payment_data->paid_status]);

            }

            $payment_response = PaymentRepo::subscriptions_payment_save($request, $subscription_details)->getData();
            
            if($payment_response->success) {
                
                DB::commit();

                $code = 123;

                return $this->sendResponse(api_success($code), $code, $payment_response->data);

            } else {

                throw new Exception($payment_response->error, $payment_response->error_code);
                
            }
        
        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

    /** 
     * @method subscriptions_autorenewal_enable()
     *
     * @uses pay for subscription using paypal
     *
     * @created Vidhya R
     *
     * @updated Vidhya R
     *
     * @param
     * 
     * @return JSON response
     *
     */

    public function subscriptions_autorenewal_enable(Request $request) {

        try {

            DB::beginTransaction();

            $subscription_payment = SubscriptionPayment::where('user_id', $request->id)->where('is_current_subscription', YES)->first();

            if(!$subscription_payment) {

                throw new Exception(api_error(137), 137);
                
            }

            $subscription_payment->is_autorenewal = YES;

            if($subscription_payment->save()) {
                
                DB::commit();

                return $this->sendResponse(api_success(117), 117, $subscription_payment);

            }

            throw new Exception(api_error(129), 129); 

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

    /** 
     * @method subscriptions_autorenewal_cancel()
     *
     * @uses pay for subscription using paypal
     *
     * @created Vidhya R
     *
     * @updated Vidhya R
     *
     * @param
     * 
     * @return JSON response
     *
     */

    public function subscriptions_autorenewal_cancel(Request $request) {

        try {

            DB::beginTransaction();

            $subscription_payment = SubscriptionPayment::where('user_id', $request->id)->where('is_current_subscription', YES)->first();

            if(!$subscription_payment) {

                throw new Exception(api_error(137), 137);
                
            }

            $subscription_payment->is_autorenewal = NO;

            if($subscription_payment->save()) {
                
                DB::commit();

                return $this->sendResponse(api_success(118), 118, $subscription_payment);

            }

            throw new Exception(api_error(130), 130); 

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

    /**
     * @method cards_list()
     *
     * @uses get the user payment mode and cards list
     *
     * @created Vithya R
     *
     * @updated Vithya R
     *
     * @param integer id
     * 
     * @return
     */

    public function cards_list(Request $request) {

        try {

            $user_cards = UserCard::where('user_id' , $request->id)->select('id as user_card_id' , 'customer_id' , 'last_four' ,'card_holder_name', 'card_token' , 'is_default' )->get();

            $payment_modes = [];

            $card_data['name'] = "Card";

            $card_data['payment_mode'] = CARD;

            $card_data['is_default'] = $this->loginUser->payment_mode == CARD ? YES : NO;

            array_push($payment_modes , $card_data);

            $data['payment_modes'] = $payment_modes;   

            $data['cards'] = $user_cards ? $user_cards : []; 

            return $this->sendResponse($message = "", $code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());

        }
    
    }
    
    /**
     * @method cards_add()
     *
     * @uses Update the selected payment mode 
     *
     * @created Vithya R
     *
     * @updated Vithya R
     *
     * @param Form data
     * 
     * @return JSON Response
     */

    public function cards_add(Request $request) {

        try {

            if(Setting::get('stripe_secret_key')) {

                \Stripe\Stripe::setApiKey(Setting::get('stripe_secret_key'));

            } else {

                throw new Exception(api_error(133), 133);
            }
        
            $rules =
                    [
                        'card_token' => 'required',
                    ];

            Helper::custom_validator($request->all(),$rules);
            
            $user_details = User::find($request->id);

            if(!$user_details) {

                throw new Exception(api_error(1002), 1002);
                
            }

            DB::beginTransaction();

            // Get the key from settings table
            
            $customer = \Stripe\Customer::create([
                // "card" => $request->card_token,
                // "card" => 'tok_visa',
                "email" => $user_details->email,
                "description" => "Customer for ".Setting::get('site_name'),
                // 'payment_method' => $request->card_token,
                // 'default_payment_method'
                // 'source' => $request->card_token
            ]);

            $stripe = new \Stripe\StripeClient(Setting::get('stripe_secret_key'));

            $intent = \Stripe\SetupIntent::create([
              'customer' => $customer->id,
              'payment_method' => $request->card_token
            ]);

            $stripe->setupIntents->confirm($intent->id,['payment_method' => $request->card_token]);


            $retrieve = $stripe->paymentMethods->retrieve($request->card_token, []);
            
            $card_info_from_stripe = $retrieve->card ? $retrieve->card : [];

            if($customer && $card_info_from_stripe) {

                $customer_id = $customer->id;

                $card_details = new UserCard;

                $card_details->user_id = $request->id;

                $card_details->customer_id = $customer_id;

                $card_details->card_token = $request->card_token ?? "NO-TOKEN";

                $card_details->card_type = $card_info_from_stripe->brand ?? "";

                $card_details->last_four = $card_info_from_stripe->last4 ?? '';

                $card_details->card_holder_name = $request->card_holder_name ?: $user_details->name;

                // Check is any default is available

                $check_card_details = UserCard::where('user_id',$request->id)->count();

                $card_details->is_default = $check_card_details ? 0 : 1;

                if($card_details->save()) {

                    if($user_details) {

                        $user_details->user_card_id = $check_card_details ? $user_details->user_card_id : $card_details->id;

                        $user_details->save();
                    }

                    $data = UserCard::where('id' , $card_details->id)->select('id as user_card_id' , 'customer_id' , 'last_four' ,'card_holder_name', 'card_token' , 'is_default' )->first();

                    DB::commit();

                    return $this->sendResponse(api_success(105), $code = 105, $data);

                } else {

                    throw new Exception(api_error(117), 117);
                    
                }
           
            } else {

                throw new Exception(api_error(117) , 117);
                
            }

        } catch(Stripe_CardError | Stripe_InvalidRequestError | Stripe_AuthenticationError | Stripe_ApiConnectionError | Stripe_Error $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode() ?: 101);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode() ?: 101);
        }
   
    }

    /**
     * @method cards_delete()
     *
     * @uses delete the selected card
     *
     * @created Vithya R
     *
     * @updated Vithya R
     *
     * @param integer user_card_id
     * 
     * @return JSON Response
     */

    public function cards_delete(Request $request) {

        // Log::info("cards_delete");

        DB::beginTransaction();

        try {
    
            $user_card_id = $request->user_card_id;

            $rules = 
                [
                    'user_card_id' => 'required|integer|exists:user_cards,id,user_id,'.$request->id,
                ];
            $custom_errors = [
                    'exists' => 'The :attribute doesn\'t belong to user:'.$this->loginUser->name
                ];

            Helper::custom_validator($request->all(),$rules);

            $user_details = User::find($request->id);

            UserCard::where('id',$user_card_id)->delete();

            if($user_details) {

                if($user_details->payment_mode = CARD) {

                    // Check he added any other card

                    if($check_card = UserCard::where('user_id' , $request->id)->first()) {

                        $check_card->is_default =  DEFAULT_TRUE;

                        $user_details->user_card_id = $check_card->id;

                        $check_card->save();

                    } else { 

                        $user_details->payment_mode = COD;

                        $user_details->user_card_id = DEFAULT_FALSE;
                        
                    }
                   
                }

                // Check the deleting card and default card are same

                if($user_details->user_card_id == $user_card_id) {

                    $user_details->user_card_id = DEFAULT_FALSE;

                    $user_details->save();
                }
                
                $user_details->save();
            
            }

            $response_array = ['success' => true , 'message' => api_success(109) , 'code' => 109];

            DB::commit();
    
            return response()->json($response_array , 200);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    }

    /**
     * @method cards_default()
     *
     * @uses update the selected card as default
     *
     * @created Vithya R
     *
     * @updated Vithya R
     *
     * @param integer id
     * 
     * @return JSON Response
     */
    public function cards_default(Request $request) {

        try {

            DB::beginTransaction();

            $rules =
                [
                    'user_card_id' => 'required|integer|exists:user_cards,id,user_id,'.$request->id,
                ];
            $custom_errors =  
                [
                    'exists' => 'The :attribute doesn\'t belong to user:'.$this->loginUser->name
                ];

            Helper::custom_validator($request->all(),$rules, $custom_errors);

            $old_default_cards = UserCard::where('user_id' , $request->id)->where('is_default', DEFAULT_TRUE)->update(['is_default' => DEFAULT_FALSE]);

            $card = UserCard::where('id' , $request->user_card_id)->update(['is_default' => DEFAULT_TRUE]);

            $user_details = User::find($request->id);

            $user_details->user_card_id = $request->user_card_id;

            $user_details->save();           

            DB::commit();

            return $this->sendResponse($message = api_success(108), $success_code = "108", $data = []);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    } 

    /**
     * @method payment_mode_default()
     *
     * @uses update the selected card as default
     *
     * @created Vithya R
     *
     * @updated Vithya R
     *
     * @param integer id
     * 
     * @return JSON Response
     */
    public function payment_mode_default(Request $request) {

        Log::info("payment_mode_default");

        try {

            DB::beginTransaction();

            $rules = 
                [
                    'payment_mode' => 'required',
                ];

            Helper::custom_validator($request->all(),$rules);

            $user_details = User::find($request->id);

            $user_details->payment_mode = $request->payment_mode ?: CARD;

            $user_details->save();           

            DB::commit();

            return $this->sendResponse($message = api_success(124), $code = 200, $data = ['payment_mode' => $request->payment_mode]);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    } 

    /** 
     * @method dashboard()
     *
     * @uses To display the user details based on user  id
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request - User Id
     *
     * @return json response with user details
     */

    public function dashboard(Request $request) {

        try {

            $base_query = $query = \App\Meeting::where('user_id', $request->id);

            $today_query = \App\Meeting::where('user_id', $request->id)->whereDate('created_at', Carbon::today());

            $data['total_meetings'] = $base_query->count();

            $data['total_completed_meetings'] = $query->where('status', MEETING_ENDED)->count();

            $data['total_today_meetings'] = $today_query->count();

            $data['today_scheduled_meetings'] = $today_query->whereIn('status', [MEETING_SCHEDULED])->count();

            $recent_meetings = $base_query->orderBy('created_at', 'desc')->skip(0)->take(3)->get();

            $data['recent_meetings'] = \App\Repositories\MeetingRepository::meetings_list_response($recent_meetings, $request);
    
            $last_x_days_meetings = \App\Repositories\MeetingRepository::last_x_days_meetings($request, 7);

            $data['last_x_days_meetings'] = $last_x_days_meetings;

            $last_x_days_meetings_website = [];

            foreach ($last_x_days_meetings as $key => $value) {

                $x_data = [$value->date, $value->total];

                array_push($last_x_days_meetings_website, $x_data);

            }

            $data['last_x_days_meetings_website'] = $last_x_days_meetings_website;

            $subscription_payment = \App\SubscriptionPayment::where('user_id', $request->id)->where('is_current_subscription', YES)->first();

            if($subscription_payment) {

                $subscription_payment->expiry_date = common_date($subscription_payment->expiry_date, $this->timezone, 'd M Y');
            }

            $data['current_subscription'] = $subscription_payment;

            return $this->sendResponse($message = "", $success_code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());

        }
    
    }


}
