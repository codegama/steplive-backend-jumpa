@extends('layouts.admin') 

@section('content-header', tr('users'))

@section('bread-crumb')

    <li class="breadcrumb-item">
    	<a href="{{ route('admin.users.index') }}">{{tr('users')}}</a>
    </li>
    <li class="breadcrumb-item active" aria-current="page">
    	<span>{{tr('add_user')}}</span>
    </li>
           
@endsection 

@section('content') 

	@include('admin.users._form') 

@endsection