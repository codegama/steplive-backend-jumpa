@extends('layouts.admin')

@section('content-header', tr('users'))

@section('bread-crumb')

<li class="breadcrumb-item"><a href="{{ route('admin.users.index')}}">{{tr('users')}}</a></li>

<li class="breadcrumb-item active" aria-current="page">
    <span>{{ tr('view_users') }}</span>
</li>

@endsection

@section('content')

<div class="row">

    <div class="col-12">

        <div class="card">

            <div class="card-body">

                <div class="border-bottom pb-3">


                    <h5 class="text-uppercase">{{tr('view_users')}}

                        <a class="btn btn-outline-primary float-right user_add" href="{{route('admin.users.create')}}">
                            <i class="fa fa-plus"></i> {{tr('add_user')}}
                        </a>

                        <div class="dropdown float-right">
                            <button class="btn btn-outline-primary  dropdown-toggle float-right bulk-action-dropdown text-uppercase" href="#" id="dropdownMenuOutlineButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-plus"></i> {{tr('bulk_action')}}
                            </button>

                            <div class="dropdown-menu float-right" aria-labelledby="dropdownMenuOutlineButton2">

                                <a class="dropdown-item action_list" href="#" id="bulk_delete">
                                    {{tr('delete')}}
                                </a>

                                <a class="dropdown-item action_list" href="#" id="bulk_approve">
                                    {{ tr('approve') }}
                                </a>

                                <a class="dropdown-item action_list" href="#" id="bulk_decline">
                                    {{ tr('decline') }}
                                </a>
                            </div>
                        </div>

                        <div class="bulk_action">

                            <form action="{{route('admin.users.bulk_action')}}" id="users_form" method="POST" role="search">

                                @csrf

                                <input type="hidden" name="action_name" id="action" value="">

                                <input type="hidden" name="selected_users" id="selected_ids" value="">

                                <input type="hidden" name="page_id" id="page_id" value="{{ (request()->page) ? request()->page : '1' }}">

                            </form>

                        </div>


                    </h5>


                </div>

                @include('admin.users._search')

                <table id="custom-datatable" class="table dt-responsive nowrap">

                    <thead>
                        <tr>
                            <th>
                                <input id="check_all" type="checkbox" class="chk-box-left">
                            </th>
                            <th>{{tr('s_no')}}</th>
                            <th>{{tr('name')}}</th>
                            <th>{{tr('email')}}</th>
                            <th>{{tr('mobile')}}</th>
                            <th>{{tr('status')}}</th>
                            <th>{{tr('verify')}}</th>
                            <th>{{tr('action')}}</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($users as $i => $user_details)

                        <tr>
                            <td id="check{{$user_details->id}}"><input type="checkbox" name="row_check" class="faChkRnd chk-box-inner-left" id="{{$user_details->id}}" value="{{$user_details->id}}"></td>

                            <td>{{$i+$users->firstItem()}}</td>

                            <td>
                                <a href="{{route('admin.users.view' , ['user_id' => $user_details->id])}}"> {{ $user_details->name }}
                                </a>
                            </td>

                            <td> {{ $user_details->email }} </td>

                            <td> {{ ($user_details->mobile > 1) ? $user_details->mobile : tr('unavailable')}} </td>

                            <td>

                                @if($user_details->status == USER_APPROVED)

                                <span class="badge badge-success">{{ tr('approved') }} </span>

                                @else

                                <span class="badge badge-danger">{{ tr('declined') }} </span>

                                @endif

                            </td>

                            <td>

                                @if($user_details->is_verified == USER_EMAIL_VERIFIED)

                                <span class="badge badge-success">{{ tr('verified') }} </span>

                                @else

                                <a class="badge badge-info" href="{{ route('admin.users.verify', ['user_id' => $user_details->id]) }}">
                                    {{ tr('verify') }}
                                </a>

                                @endif

                            </td>

                            <td>

                                <div class="template-demo">

                                    <div class="dropdown">

                                        <button class="btn btn-outline-primary  dropdown-toggle" type="button" id="dropdownMenuOutlineButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            {{tr('action')}}
                                        </button>

                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuOutlineButton1">

                                            <a class="dropdown-item" href="{{ route('admin.users.view', ['user_id' => $user_details->id]) }}">
                                                {{tr('view')}}
                                            </a>

                                            @if(Setting::get('is_demo_control_enabled') == NO)
                                            <a class="dropdown-item" href="{{ route('admin.users.edit', ['user_id' => $user_details->id]) }}">
                                                {{tr('edit')}}
                                            </a>

                                            <a class="dropdown-item" href="{{route('admin.users.delete', ['user_id' => $user_details->id,'page'=>request()->input('page')])}}" onclick="return confirm(&quot;{{tr('user_delete_confirmation' , $user_details->name)}}&quot;);">
                                                {{tr('delete')}}
                                            </a>
                                            @else

                                            <a class="dropdown-item text-muted" href="javascript:;">{{tr('edit')}}</a>

                                            <a class="dropdown-item text-muted" href="javascript:;">{{tr('delete')}}</a>
                                            @endif

                                            <div class="dropdown-divider"></div>

                                            @if($user_details->is_verified == USER_EMAIL_NOT_VERIFIED)

                                            <a class="dropdown-item" href="{{ route('admin.users.verify', ['user_id' => $user_details->id]) }}"> {{ tr('verify') }}
                                            </a>

                                            @endif

                                            @if($user_details->status == USER_APPROVED)

                                            <a class="dropdown-item" href="{{ route('admin.users.status', ['user_id' => $user_details->id]) }}" onclick="return confirm(&quot;{{$user_details->first_name}} - {{tr('user_decline_confirmation')}}&quot;);">
                                                {{ tr('decline') }}
                                            </a>

                                            @else

                                            <a class="dropdown-item" href="{{ route('admin.users.status', ['user_id' => $user_details->id]) }}">
                                                {{ tr('approve') }}
                                            </a>

                                            @endif

                                            @if($user_details->is_verified == USER_EMAIL_VERIFIED)

                                            <a class="dropdown-item" href="{{ route('admin.users.verify',['user_id' => $user_details->id] ) }}">
                                                {{ tr('unverify') }}
                                            </a>
                                            @else

                                            <a class="dropdown-item" href="{{ route('admin.users.verify',['user_id' => $user_details->id] ) }}">
                                                {{ tr('verify') }}
                                            </a>

                                            @endif

                                            <a class="dropdown-item" href="{{route('admin.users.subscriptions_index',['user_id' => $user_details->id])}}">{{tr('subscriptions')}}</a>

                                            <a class="dropdown-item" href="{{route('admin.meetings.index',['user_id' => $user_details->id])}}">{{tr('view_meetings')}}</a>

                                        </div>

                                    </div>

                                </div>

                            </td>

                        </tr>

                        @endforeach
                    </tbody>

                </table>

                <div class="float-right" id="paglink">{{ $users->appends(request()->input())->links() }}</div>

            </div>

        </div>

    </div>

</div>




@endsection

@section('scripts')

@if(Session::has('bulk_action'))
<script type="text/javascript">
    $(document).ready(function() {
        localStorage.clear();
    });
</script>
@endif

<script type="text/javascript">
    $(document).ready(function() {
        get_values();

        // Call to Action for Delete || Approve || Decline
        $('.action_list').click(function() {
            var selected_action = $(this).attr('id');
            if (selected_action != undefined) {
                $('#action').val(selected_action);
                if ($("#selected_ids").val() != "") {
                    if (selected_action == 'bulk_delete') {
                        var message = "{{ tr('admin_users_delete_confirmation') }}";
                    } else if (selected_action == 'bulk_approve') {
                        var message = "{{ tr('admin_users_approve_confirmation') }}";
                    } else if (selected_action == 'bulk_decline') {
                        var message = "{{ tr('admin_users_decline_confirmation') }}";
                    }
                    var confirm_action = confirm(message);

                    if (confirm_action == true) {
                        $("#users_form").submit();
                    }
                    // 
                } else {
                    alert('Please select the check box');
                }
            }
        });
        // single check
        var page = $('#page_id').val();
        $('.faChkRnd:checkbox[name=row_check]').on('change', function() {

            var checked_ids = $(':checkbox[name=row_check]:checked').map(function() {
                    return this.id;
                })
                .get();

            localStorage.setItem("user_checked_items" + page, JSON.stringify(checked_ids));

            get_values();

        });
        // select all checkbox
        $("#check_all").on("click", function() {
            if ($("input:checkbox").prop("checked")) {
                $("input:checkbox[name='row_check']").prop("checked", true);
                var checked_ids = $(':checkbox[name=row_check]:checked').map(function() {
                        return this.id;
                    })
                    .get();
                // var page = {!! $users->lastPage() !!};
                console.log("user_checked_items" + page);

                localStorage.setItem("user_checked_items" + page, JSON.stringify(checked_ids));
                get_values();
            } else {
                $("input:checkbox[name='row_check']").prop("checked", false);
                localStorage.removeItem("user_checked_items" + page);
                get_values();
            }

        });

        // Get Id values for selected User
        function get_values() {
            var pageKeys = Object.keys(localStorage).filter(key => key.indexOf('user_checked_items') === 0);
            var values = Array.prototype.concat.apply([], pageKeys.map(key => JSON.parse(localStorage[key])));

            if (values) {
                $('#selected_ids').val(values);
            }

            for (var i = 0; i < values.length; i++) {
                $('#' + values[i]).prop("checked", true);
            }
        }

        $('.faChkRnd').on("click", function() {

            var value = $(this).val();

            $('#check' + value).trigger('click');
        });

    });
</script>

@endsection