@extends('layouts.admin') 

@section('content-header',tr('support_contacts'))

@section('title', tr('support_contacts'))

@section('bread-crumb')

    <li class="breadcrumb-item active" aria-current="page">
        <span>{{ tr('support_contacts') }}</span>
    </li>
           
@endsection 

@section('content')

<div class="col-lg-12 grid-margin stretch-card">
        
    <div class="card">

        <div class="card-body">

            <div class="border-bottom pb-3">

                <h5 class="text-uppercase">{{tr('support_contacts')}}</h5>

                      <div class="dropdown float-right top-btn">
                            <button class="btn btn-outline-primary  dropdown-toggle float-right bulk-action-dropdown text-uppercase" href="#" id="dropdownMenuOutlineButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-plus"></i> {{tr('bulk_action')}}
                            </button>

                            <div class="dropdown-menu float-right" aria-labelledby="dropdownMenuOutlineButton2">

                                <a class="dropdown-item action_list" href="#" id="bulk_delete">
                                    {{tr('delete')}}
                                </a>

                                <a class="dropdown-item action_list" href="#" id="bulk_approve">
                                    {{ tr('approve') }}
                                </a>

                                <a class="dropdown-item action_list" href="#" id="bulk_decline">
                                    {{ tr('decline') }}
                                </a>
                            </div>
                        </div>

                        <div class="bulk_action">

                            <form action="{{route('admin.support_contacts.bulk_action')}}" id="support_contacts_form" method="POST" role="search">

                                @csrf

                                <input type="hidden" name="action_name" id="action" value="">

                                <input type="hidden" name="selected_contacts" id="selected_ids" value="">

                                <input type="hidden" name="page_id" id="page_id" value="{{ (request()->page) ? request()->page : '1' }}">

                            </form>

                        </div>

            </div>

            <div class="table-responsive pb-3 pt-3">
                 @include('admin.support_contacts._search')
                <table id="custom-datatable" class="table dt-responsive nowrap">
                    <thead>
                        <tr>
                        <th>
                                <input id="check_all" type="checkbox" class="chk-box-left">
                            </th>
                            <th>{{tr('s_no')}}</th>
                            <th>{{tr('name')}}</th>
                            <th>{{tr('email')}}</th>
                            <th>{{tr('mobile')}}</th>
                            <th>{{tr('country')}}</th>
                            <th>{{tr('status')}}</th>
                            <th>{{tr('action')}}</th>
                        </tr>
                    </thead>

                    <tbody>


                        @foreach($support_contacts as $i => $support_contact)

                            <tr>
                               <td id="check{{$support_contact->id}}"><input type="checkbox" name="row_check" class="faChkRnd chk-box-inner-left" id="{{$support_contact->id}}" value="{{$support_contact->id}}"></td>

                                <td>{{$i+$support_contacts->firstItem()}}</td>

                                <td>
                                    <a href="{{route('admin.support_contacts.view' , ['support_contact_id' => $support_contact->id] )}}"> {{$support_contact->name}}</a>
                                </td>

                                <td>{{$support_contact->email}}</td>
                                
                                <td>{{$support_contact->mobile}}</td>

                                <td>{{$support_contact->country}}</td>

                                <td>
                                @if($support_contact->status == APPROVED)

                                <span class="badge badge-success">{{ tr('approved') }} </span>

                                @else

                                <span class="badge badge-danger">{{ tr('declined') }} </span>

                                @endif
                                </td>


                                <td>
                                    <a class="btn btn-success" href="{{route('admin.support_contacts.view', ['support_contact_id' =>  $support_contact->id] )}}">
                                        {{tr('view')}}
                                    </a>
                                </td>
                            
                            </tr>

                        @endforeach

                    </tbody>

                </table>

                <div class="float-right">{{$support_contacts->links()}}</div>

            </div>

        </div>
    
    </div>

</div>

@endsection

@section('scripts')

@if(Session::has('bulk_action'))
<script type="text/javascript">
    $(document).ready(function() {
        localStorage.clear();
    });
</script>
@endif

<script type="text/javascript">
    $(document).ready(function() {
        get_values();

        // Call to Action for Delete || Approve || Decline
        $('.action_list').click(function() {
            var selected_action = $(this).attr('id');
            if (selected_action != undefined) {
                $('#action').val(selected_action);
                if ($("#selected_ids").val() != "") {
                    if (selected_action == 'bulk_delete') {
                        var message = "{{ tr('admin_support_contacts_delete_confirmation') }}";
                    } else if (selected_action == 'bulk_approve') {
                        var message = "{{ tr('admin_support_contacts_approve_confirmation') }}";
                    } else if (selected_action == 'bulk_decline') {
                        var message = "{{ tr('admin_support_contacts_decline_confirmation') }}";
                    }
                    var confirm_action = confirm(message);

                    if (confirm_action == true) {
                        $("#support_contacts_form").submit();
                    }
                    // 
                } else {
                    alert('Please select the check box');
                }
            }
        });
        // single check
        var page = $('#page_id').val();
        $('.faChkRnd:checkbox[name=row_check]').on('change', function() {

            var checked_ids = $(':checkbox[name=row_check]:checked').map(function() {
                    return this.id;
                })
                .get();

            localStorage.setItem("support_contact_checked_items" + page, JSON.stringify(checked_ids));

            get_values();

        });
        // select all checkbox
        $("#check_all").on("click", function() {
            if ($("input:checkbox").prop("checked")) {
                $("input:checkbox[name='row_check']").prop("checked", true);
                var checked_ids = $(':checkbox[name=row_check]:checked').map(function() {
                        return this.id;
                    })
                    .get();

                console.log("support_contact_checked_items" + page);

                localStorage.setItem("support_contact_checked_items" + page, JSON.stringify(checked_ids));
                get_values();
            } else {
                $("input:checkbox[name='row_check']").prop("checked", false);
                localStorage.removeItem("support_contact_checked_items" + page);
                get_values();
            }

        });

        // Get Id values for selected User
        function get_values() {
            var pageKeys = Object.keys(localStorage).filter(key => key.indexOf('support_contact_checked_items') === 0);
            var values = Array.prototype.concat.apply([], pageKeys.map(key => JSON.parse(localStorage[key])));

            if (values) {
                $('#selected_ids').val(values);
            }

            for (var i = 0; i < values.length; i++) {
                $('#' + values[i]).prop("checked", true);
            }
        }

        $('.faChkRnd').on("click", function() {

            var value = $(this).val();

            $('#check' + value).trigger('click');
        });

    });

   
</script>

@endsection


@section('styles')
<style>
    .table.dataTable.dtr-inline.collapsed > tbody > tr[role="row"] > td:first-child::before{
        
        display:none;
    }

  
</style>
@endsection